// Следить за обновлением 
// на https://github.com/gulpjs/gulp-util/issues/143 
// Обновить версию gulp-pug и gulp-sass сразу же после выхода релиза

import gulp from 'gulp';
import babel from 'gulp-babel';
import autoprefixer from 'gulp-autoprefixer';
import cache from 'gulp-cache';
import csso from 'gulp-csso'; 
import sass from 'gulp-sass';
import uglify from 'gulp-uglify';
import concat from 'gulp-concat';
import connect from 'gulp-connect';
import debug from 'gulp-debug';
import pug from 'gulp-pug';
import plumber from 'gulp-plumber';
import sourcemaps from 'gulp-sourcemaps';
import del from 'del';
import conf from './config/path';
import Pageres from 'pageres';
import moduleImporter from 'sass-module-importer';

// TESTS
var pageres = new Pageres({delay: 5})
    .src('localhost:3000', [
        '320x568', 
        '360x640',
        '768x1024',
        '1280x768',
        '1600x1024',
        '1920x1080',
        '2048x1080'
        ], {crop: true})
    .dest('test/screenshots/');

export function screenshots() {
    pageres.run(function (err) {
        if (err) {
            throw err;
        }
        console.log('done');
    });
}
// END TESTS

// CLEAN
export const clean = () => del(conf.path.basr_dir); 
export const cleanCache = () => cache.clearAll(); 

// SERVER
export function connectDist(cb) {
    connect.server({
        root: ['build'],
        port: 3000,
        livereload: true
    });
    cb();
}

// HTML BUILD
export function html() {
    return gulp.src(conf.path.html.src.main)
        .pipe(plumber())
        .pipe(pug({
            pretty: '    '
        }))
        .pipe(debug({title: 'Главня страница:'}))

        .pipe(gulp.dest(conf.path.html.dist.main))
        .pipe(connect.reload());
}

export function htmlPage () {
    return gulp.src(conf.path.html.src.page)
        .pipe(plumber())
        .pipe(pug({
            pretty: '    '
        }))
        .pipe(debug({title: 'Страницы html:'}))

        .pipe(gulp.dest(conf.path.html.dist.page))
        .pipe(connect.reload());
}


// STYLE
export function style() {
    return gulp.src(conf.path.style.src)
        .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(sass({
            includePaths: conf.path.node.sass,
            importer: moduleImporter(),
            sourceMap: true
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(csso())
        .pipe(sourcemaps.write())
        .pipe(debug({title: 'css:'}))
        .pipe(gulp.dest(conf.path.style.dist))
        .pipe(connect.reload());
}

// SCRIPTS
export function scripts() {
  return gulp.src(conf.path.scripts.src)
    .pipe(sourcemaps.init())
      .pipe(babel({
          presets: ['env']
      }))
    .pipe(uglify())
    .pipe(concat('main.min.js'))
    .pipe(gulp.dest(conf.path.scripts.dist))
    .pipe(connect.reload())
    .pipe(sourcemaps.write())
}

//IMAGE
export function image() {
  return gulp.src(conf.path.image.src) 
    .pipe(gulp.dest(conf.path.image.dist))
    .pipe(connect.reload())
    .pipe(sourcemaps.write())
}

// Fonts
export function fonts() {
  return gulp.src(conf.path.fonts.src)
    .pipe(gulp.dest(conf.path.fonts.dist))
    .pipe(connect.reload())
    .pipe(sourcemaps.write())
}


// WATCH
function watchFiles(cb) {
    gulp.watch(conf.path.html.watch, html);
    gulp.watch(conf.path.style.watch, style);
    gulp.watch(conf.path.scripts.watch, scripts);
    gulp.watch(conf.path.image.src, image);
    gulp.watch(conf.path.fonts.src, fonts);
    cb();
}

export { watchFiles as watch };

const build = gulp.series(
    clean, 
    cleanCache,
    scripts,
    image,
    fonts,
    gulp.parallel(
        html, 
        htmlPage,
        style,
        connectDist,
        watchFiles
        )
    );

const test = gulp.series(screenshots); // Пока не корректно работает

gulp.task('build', gulp.series(build)); 
gulp.task('test', gulp.series(test));

export default build;