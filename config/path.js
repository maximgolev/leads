var path = {
    basr_dir: './build/',
    html: {
        src: {
            main: './src/app/index.pug',
            page: [
                './src/layout/*.pug',
                './src/page/**/*.pug'
            ],
            components: './src/components/**/*.pug',
            blocks: './src/blocks/**/*.pug'
        },
        dist: {
            main: './build',
            page: './build/page/'
        },
        watch:[
            './src/layout/*.pug',
            './src/app/*.pug',
            './config/*.pug',
            './src/page/**/*.pug',
            './src/blocks/**/*.pug',
            './src/components/**/*.pug'
        ]
    },
    node: {
        sass: [
            'node_modules/bootstrap/scss/'
        ]
    },
    style: {
        src: './src/app/style.scss',
        dist: './build/assets/css/',
        watch: [
            './src/app/*.scss',
            './src/assets/sass/*.scss',
            './src/blocks/**/*.scss',
            './src/page/**/*.scss',
            './src/components/**/*.scss'
        ]
    },
    scripts: {
        src: [
            './src/app/*.js',
            './src/assets/js/*.js',
            './src/components/**/*.js',
            './src/blocks/**/*.js'
        ],
        dist: './build/assets/js/',
        watch: [
            './src/app/*.js',
            './src/assets/js/*.js',
            './src/components/**/*.js',
            './src/blocks/**/*.js',
            './config/*.pug',
            './config/*.js'
        ]
    },
    image: {
        src: './src/assets/img/**/*.*',
        dist: './build/assets/img/'
    },
    fonts: {
        src: [
            './src/assets/fonts/**/*.*',
            '!./src/assets/fonts/FontAwesome/scss/**'
            ],
        dist: './build/assets/fonts/'
    }
};

module.exports = {path: path};